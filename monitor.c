#include <stdatomic.h>
#include <time.h>
#include <windows.h>

static atomic_llong last_input_time = 0;

HHOOK hKeyboardHook = NULL;

LRESULT CALLBACK KeyboardHookProc(int nCode, WPARAM wParam, LPARAM lParam) {
  if (nCode == HC_ACTION) {
    atomic_store(&last_input_time, time(NULL));
  }

  return CallNextHookEx(hKeyboardHook, nCode, wParam, lParam);
}

__declspec(dllexport) void start_hook(void) {
  if (!hKeyboardHook) {
    hKeyboardHook = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardHookProc, NULL, 0);
  }
}

__declspec(dllexport) void stop_hook(void) {
  if (hKeyboardHook) {
    UnhookWindowsHookEx(hKeyboardHook);
    hKeyboardHook = NULL;
  }
}

__declspec(dllexport) long long last_input_received(void) {
  return atomic_load(&last_input_time);
}

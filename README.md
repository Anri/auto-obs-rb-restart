# Automatic OBS ReplayBuffer Restart [![status-badge](https://git.mylloon.fr/Anri/auto-obs-rb-restart/badges/workflows/release.yml/badge.svg)](https://git.mylloon.fr/Anri/auto-obs-rb-restart/actions?workflow=release.yml)

Automatically restarts the replay buffer when IDLE

- [Download the latest release here](https://git.mylloon.fr/Anri/auto-obs-rb-restart/releases/tag/latest)

The script wait 2 hours before looking for any input for the last 2 minutes.
If there is none, then it restarts the replay buffer.
Else it waits for the next 2 minutes, etc.
Once restarted, the script wait 2 other hours.

obs = obslua

local wait_time = 7200 -- Wait 2 hours before checks
local idle_time = 120  -- Wait for any input in the last 2 minutes

function script_description()
  return [[
  Automatically restarts the replay buffer when IDLE
  Author: Mylloon
  ]]
end

function script_load()
  ffi = require("ffi")
  ffi.cdef[[
      void start_hook(void);
      void stop_hook(void);
      long long last_input_received(void);
    ]]
  monitor = ffi.load(script_path() .. "monitor.dll")
  start_loop()
end

function script_unload()
  obs.timer_remove(check_restart)
  obs.timer_remove(check_idle)
  monitor.stop_hook()
end

function start_loop()
  print("Start idle")
  obs.timer_add(check_restart, wait_time * 1000)
end


function check_restart()
  obs.timer_remove(check_restart)

  print("Start check every " .. idle_time .. "secs")

  monitor.start_hook()
  obs.timer_add(check_idle, idle_time * 1000)
end

function check_idle()
  obs.timer_remove(check_idle)
  local current_time = os.time()
  local last_input_time = monitor.last_input_received()

  if current_time - last_input_time >= idle_time then
    monitor.stop_hook()

    print("No activity detected, restart replay buffer")
    obs.obs_frontend_replay_buffer_stop()
    obs.timer_add(start_replay_buffer, 4000)
  else
    obs.timer_add(check_idle, idle_time * 1000)
  end
end

function start_replay_buffer()
  obs.timer_remove(start_replay_buffer)
  obs.obs_frontend_replay_buffer_start()

  start_loop()
end
